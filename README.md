Glenda A Smith, located in Cincinnati, OH, has been practicing law since 1992. She is licensed to practice law in both Indiana and Ohio. Glenda graduated from Indiana University at Indianapolis. Glenda also has a Bachelor of Science in Criminal Justice, Masters of Public Affairs and Doctorate of Jurisprudence.

Website : https://gasmithlaw.com
